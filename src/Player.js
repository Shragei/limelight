//Copyright (c) James Schroer, 2014 
function Player(images,width,height,loop){
  var canvas=document.createElement('canvas');
  canvas.height=height;
  canvas.width=width;
  canvas.style.height=canvas.height;
  canvas.style.width=canvas.width;
  
  var canvasContext=canvas.getContext('2d');
  
  var previousFrame=document.createElement('canvas');
  previousFrame.height=height;
  previousFrame.width=width;
  previousFrame.style.height=previousFrame.height;
  previousFrame.style.width=previousFrame.width;
  var previousFrameContext=previousFrame.getContext('2d');

  var currentFrame=0;
  var loopCount=0;
  var timeOut=0;
  var dispose='none';

  this.Reset=function(){
    this.Seek(0);
  }
  this.Seek=function(frame){
    canvasContext.clearRect(0,0,canvas.width,canvas.height);  
    previousFrameContext.clearRect(0,0,canvas.width,canvas.height);   
    
    for(currentFrame=0;currentFrame<=frame;currentFrame++)
      showFrame();
  }
  this.Play=function(){
    if(timeOut==0)
      animate();
  }
  this.Stop=function(){
    this.Pause();
    this.Reset();
  }
  this.Pause=function(){
    window.clearTimeout(timeOut);
    timeOut=0;
  }
  
  this.SetLoopCount=function(count){
    loopCount=count;
  }
  this.Canvas=function(){
    return canvas;
  }
  
  function showFrame(){
    var image=images[currentFrame];
    if(dispose=='Background')
      canvasContext.clearRect(0,0,canvas.width,canvas.height);
    
    if(image.BlendOperation=='Source')
      canvasContext.clearRect(image.XOffset,image.YOffset,
                              image.Width,image.Height);
                              
    canvasContext.drawImage(image.Data,
                            image.XOffset,image.YOffset,
                            image.Width,image.Height);
              
    previousFrameContext.clearRect(0,0,canvas.width,canvas.height);    
    previousFrameContext.drawImage(canvas,0,0,canvas.width,canvas.height);
    
    dispose=image.DisposeOperation;
  }
  
  function animate(){
    if(currentFrame>=images.length){
      currentFrame=0;
      loopCount++;
      if(loopCount>=loop)
        return;
    }
    showFrame();
    var image=images[currentFrame++];
    var delay=(image.DelayNumerator/image.DelayDenominator)*1000;
    timeOut=window.setTimeout(animate,delay);
  }
}