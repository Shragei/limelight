//Copyright (c) James Schroer, 2014 
function TIFF(){
  var Datatype={'1':'Uint8','2':'String','3':'Uint16','4':'Uint32','5':'Urational64',
                '6':'Int8','7':'Undefine','8':'Int16','9':'Int32','10':'Rational64',
                '11':'Float32','12':'Float64'};
  var tags={};
  this.Tags=function(Tag,callback){
    tags[parseInt(Tag,16)]=callback;
  }
  function IFD(count,proc){
    console.group('IFD');
    while(count--){
      var entry=proc.processStructure({Tag:'Uint16',Datatype:'Uint16',Count:'Uint32'});
      if(entry.Tag in tags){//iterate over the useful tags
        var type=Datatype[entry.Datatype];
        var size=0;
        if(type=='String'){
          size=entry.Count;
          type=type+entryCount;
        }else if(type=='Undefine')
          size=entry.Count;
        else
          size=proc.typeSize[type]*entry.Count;
          
        var pos=0;
        if(size>4){//If value of entry is bigger than 4 bytes use it as an offset and jump. 
          offset=proc.processStructure({Offset:'Uint32'}).Offset;
          pos=proc.tell();
          proc.seek(offset,true); 
        }
        var value=proc.slice(size,true);
        if(pos)
          proc.seek(pos,true);
        if(type!='Undefine'){
          var tmp=[];
          for(var i=0;i<entry.Count;i++)
            tmp.push(value.processStructure({Value:type}).Value);
          value=tmp;
        }
        
        tags[entry.Tag](entry.Datatype,entry.Count,value);
      }else{ //Not using tag, skip it.
        proc.seek(4,false);
        console.log('Tag: '+entry.Tag.toString(16)+' Datatype: '+Datatype[entry.Datatype]+' Count: '+entry.Count);
      }
      
    }
    console.groupEnd();
  }

  this.execute=function(proc){
    var endian=proc.processStructure({endian:'Uint16'}).endian;
    if(endian == 18761)
      proc.endian('little');
    else
      proc.endian('big');
    var test=proc.processStructure({life:'Uint16'}).life;
    var IFDOffset=proc.processStructure({Offset:'Uint32'}).Offset;
    do{
      proc.seek(IFDOffset,true);
      var count=proc.processStructure({Count:'Uint16'}).Count;
      var position=proc.tell();
      
      IFD(count,proc);
      
      proc.seek(position+count*12,true);
      IFDOffset=proc.processStructure({Offset:'Uint32'}).Offset;
    }while(IFDOffset>0);
  }
  
  this.pack=function(proc,struct){
    //TODO
  
  }
}