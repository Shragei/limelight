//Copyright (c) James Schroer, 2014
function APNG(url,callback){
  var CRC=new CRC32();
  var signature=new Processor(new ArrayBuffer(8));//PNG signature
  signature.writeStructure({a:'Uint32',b:'Uint32'},
                           {a:0x89504e47,b:0x0d0a1a0a});
                      
  var metadata=new Object;
  var ImageStack=new Array;
  var stack=new Array;
  var aux={head:new Array,tail:new Array};
  var chunkTypes={
    'IHDR':readIHDR,
    'IEND':readIEND,
    'IDAT':readIDAT,
    'PLTE':readPLTE,
    'acTL':readacTL,
    'fcTL':readfcTL,
    'fdAT':readfdAT
    
  };
  
  var imageState={};
  var image=new Processor(url);
  
  function chunkRead(image){
    var chunkStart=image.tell();
    var chunk=image.processStructure({Length:'Uint32',Type:'String4'});
    image.seek(chunk.Length,false);
    chunk.CRC=image.processStructure({CRC:'Uint32'}).CRC;    
    var chunkEnd=image.tell();
    image.seek(chunkStart,true);
    chunk.data=image.slice(chunkEnd-chunkStart,true);
    return chunk;
  }

  function writeBuff(chunkData,buffer,offset){
    chunkData.seek(0,true);
    var chunkbuff=chunkData.slice(chunkData.length());
    chunkbuff=new Uint8Array(chunkbuff);
    for(var i=0;i<chunkbuff.length;i++)
      buffer[offset++]=chunkbuff[i];
    return offset;
  }
  
  function readIHDR(chunk){
    if(stack.length!=0) //IHDR needs to be first. 
      return false;
      
    metadata.signature='PNG';
    
    chunk.data.seek(8,true);
    metadata=chunk.data.processStructure({Width:'Uint32',
                                          Height:'Uint32',
                                          BitDepth:'Uint8',
                                          ColorType:'Uint8',
                                          CompressMethod:'Uint8',
                                          FilterMethod:'Uint8',
                                          InterlaceMethod:'Uint8'});
    
    switch(metadata.ColorType){
      case 0:metadata.ColorType='Grayscale';break;
      case 2:metadata.ColorType='RGB';break;
      case 3:metadata.ColorType='Palette';break;
      case 4:metadata.ColorType='Grayscale+Alpha';break;
      case 6:metadata.ColorType='RGB+Alpha';break;
      default:metadata.ColorType='Unknown';break;
    }

    switch(metadata.CompressMethod){
      case 0:metadata.CompressMethod='deflate';break
      default:metadata.CompressMethod='Unknown';break;
    }
    
    switch(metadata.FilterMethod){
      case 0:metadata.FilterMethod='Adaptive';break
      default:metadata.FilterMethod='Unknown';break;
    }
    switch(metadata.InterlaceMethod){
      case 0:metadata.InterlaceMethod='None';break;
      case 1:metadata.InterlaceMethod='Adam7';break;
      default:metadata.InterlaceMethod='Unknown';break;
    }
    return true;
  }
  function readIEND(chunk){
    return false;
  }
  function readIDAT(chunk){
    if(stack.length==0)
      return false;
    
    if(metadata.signature=='JNG'){
      var set=1;
      if(stack.indexOf('JSEP')>-1)//Switch from 8bit image to 12bit
        set=3;
      
      if(!(stack[set] instanceof Array))    
        ImageStack[set]=new Array;
        
      ImageStack[set].push(chunk);
    }else{
      if(stack[stack.length-1]!='IDAT')
        ImageStack.push(new Array);
        
      ImageStack[ImageStack.length-1].push(chunk);
    }
    return true;
  }
  function readacTL(chunk){
    chunk.data.seek(8,true);
    metadata.animation=chunk.data.processStructure({FrameCount:'Uint32',
                                                    LoopCount:'Uint32'});
    if(metadata.animation.LoopCount==0)
      metadata.animation.LoopCount=Number.POSITIVE_INFINITY;
    return true;
  }
  function readPLTE(chunk){
    if(stack.legnth==0||metadata.signature=='JNG')
      return false;
    aux.head.push(chunk);
    return true;
  }
  function readfcTL(chunk){
    if(stack.length==0)
      return false;
    chunk.data.seek(8,true);
    frameMeta=chunk.data.processStructure({SequenceNumber:'Uint32',
                                           Width:'Uint32',
                                           Height:'Uint32',
                                           XOffset:'Uint32',
                                           YOffset:'Uint32',
                                           DelayNumerator:'Uint16',
                                           DelayDenominator:'Uint16',
                                           DisposeOperation:'Uint8',
                                           BlendOperation:'Uint8'});
    switch(frameMeta.DisposeOperation){
      case 0:frameMeta.DisposeOperation='None';break;
      case 1:frameMeta.DisposeOperation='Background';break;
      case 2:frameMeta.DisposeOperation='Previous';break;
      default:frameMeta.DisposeOperation='Unknown';
    }
    switch(frameMeta.BlendOperation){
      case 0:frameMeta.BlendOperation='Source';break;
      case 1:frameMeta.BlendOperation='Over';break;
      default:frameMeta.BlendOperation='Unknown';
    }

    if(!(metadata.frames instanceof Array))
      metadata.frames=new Array;
    metadata.frames.push(frameMeta);
    return true;
  }
  function readfdAT(chunk){
    if(stack.length==0)
      return false;
      
    rebuildIDAT(chunk);
    chunk.Type='IDAT';
    return readIDAT(chunk);
  }

  function rebuildIHDR(newMetadata){
    var newIHDR=new Processor(new ArrayBuffer(25));
    
    
    switch(newMetadata.ColorType){
      case 'Grayscale':newMetadata.ColorType=0;break;
      case 'RGB':newMetadata.ColorType=2;break;
      case 'Palette':newMetadata.ColorType=3;break;
      case 'Grayscale+Alpha':newMetadata.ColorType=4;break;
      case 'RGB+Alpha':newMetadata.ColorType=6;break;
    }

    switch(newMetadata.CompressMethod){
      case 'deflate':newMetadata.CompressMethod=0;break
    }
    
    switch(newMetadata.FilterMethod){
      case 'Adaptive':newMetadata.FilterMethod=0;break
    }
    switch(newMetadata.InterlaceMethod){
      case 'None':newMetadata.InterlaceMethod=0;break;
      case 'Adam7':newMetadata.InterlaceMethod=1;break;
    }
    
    newMetadata.Length=13;
    newMetadata.Type='IHDR';
    
    newIHDR.writeStructure({Length:'Uint32',
                            Type:'String4',
                            Width:'Uint32',
                            Height:'Uint32',
                            BitDepth:'Uint8',
                            ColorType:'Uint8',
                            CompressMethod:'Uint8',
                            FilterMethod:'Uint8',
                            InterlaceMethod:'Uint8'},newMetadata);
    newIHDR.seek(4,true);
    var crc=CRC.Compute(newIHDR.slice(newIHDR.length()-8));
    newIHDR.writeStructure({CRC:'Uint32'},{CRC:crc});
    newIHDR.seek(0,true);
    return newIHDR;
  }
  function rebuildIDAT(chunk){
    var data=chunk.data;
    data.seek(8,false);
    chunk.SequenceNumber=data.processStructure({SeqNum:'Uint32'})
    data.seek(4,true);
    data=data.slice(data.length()-4,true);//strip the fdAT sequence_number from the beginning of chunk
    
    chunk.Length-=4;
    
    data.writeStructure({Length:'Uint32',Type:'String4'},{Length:chunk.Length,Type:'IDAT'});//make image chunk valid
    data.seek(4,true); //reset to past length;
    chunk.CRC=CRC.Compute(data.slice(data.length()-8)); //slice everything to crc
    data.writeStructure({CRC:'Uint32'},{CRC:chunk.CRC}); //replace crc with new one
    chunk.data=data;
  }
  

  function readPNGStructure(){
    var ret;
    do{
      var chunk=chunkRead(image);
      var call=chunkTypes[chunk.Type];
      if(call!==undefined)
        ret=call(chunk);
      stack.push(chunk.Type);
    }while(ret);
  }
  
  function buildChunk(meta,data){
    var length=0;
    if(data)
      length=data.length();
    
    var chunk=new Processor(new ArrayBuffer(length+12));
    chunk.writeStructure({Length:'Uint32',Type:'String4'},meta);
   
    if(data)
      chunk.splice(data);

    chunk.seek(4,true);

    var crc=CRC.Compute(chunk.slice(chunk.length()-8));
    chunk.writeStructure({CRC:'Uint32'},{CRC:crc});
    chunk.seek(0,true);
    return chunk;
  }
  function rebuildImage(chunks,meta){
    var initSize=8+25+12; //signature+IHDR+IEND
    
    for(var i=0;i<aux.head.length;i++)
      initSize+=aux.head[i].data.length();
    for(var i=0;i<aux.tail.length;i++)
      initSize+=aux.tail[i].data.length();
      
    var size=initSize;
      
    for(var c=0;c<chunks.length;c++)//calculate chunk size
      size+=chunks[c].data.length();
      
    var buffer=new Processor(new ArrayBuffer(size));
    signature.seek(0,true);
    buffer.splice(signature);
    
    var newIHDR=rebuildIHDR(meta);
    buffer.splice(newIHDR);
    
    for(var i=0;i<aux.head.length;i++){
      aux.head[i].data.seek(0,true);
      buffer.splice(aux.head[i].data);
    }
      
    for(var c=0;c<chunks.length;c++){
      chunks[c].data.seek(0,true);
      buffer.splice(chunks[c].data);
    }
    
    for(var i=0;i<aux.tail.length;i++){
      aux.tail[i].data.seek(0,true);
      buffer.splice(aux.tail[i].data);
    }
    
    var end=buildChunk({Length:0,Type:'IEND'});
    buffer.splice(end);
    return buffer;
  }
  function frameRebuild(){
    //rebuild frames as single png images

    for(var f=0;f<ImageStack.length;f++){
      var newMetadata=new Object;
      for(var key in metadata){
        newMetadata[key]=metadata[key];
      }      
      newMetadata.Height=metadata.frames[f].Height;
      newMetadata.Width=metadata.frames[f].Width;
      ImageStack[f]=rebuildImage(ImageStack[f],newMetadata);
    }
  }

  
  image.onload=function(){
    image.seek(8,true);//Skip PNG header
    
    readPNGStructure();

    frameRebuild();
    if('animation' in metadata){
      var loaded=ImageStack.length;
      var frames=metadata.frames;
      for(var i=0;i<ImageStack.length;i++){
        var img=new Image;

        img.src=URL.createObjectURL(ImageStack[i].blob('image/png'));
        img.onload=function(){
          if(--loaded==0){
            var player=new Player(frames,
                                  metadata.Width,metadata.Height,
                                  metadata.animation.LoopCount);
            for(var i=0;i<frames.length;i++)
              URL.revokeObjectURL(frames[i].Data.src);
            callback(player);
          }
        }
        frames[i].Data=img;
      }
    }
  }
  image.execute();
}