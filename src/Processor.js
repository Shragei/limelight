//Copyright (c) James Schroer, 2014 
function Processor(url){
  var offset=0;
  var size=0;
  var buffer;
  var endianness=false; //Big endian
  
  this.onload;
  
  function encapsulate(buff){
    var sizeArray=new Uint8Array(buff);
    size=sizeArray.byteLength;
    buffer=buff;
  }
  
  if(typeof url=='number')
    url=new ArrayBuffer(url);
    
  if(url instanceof ArrayBuffer)
    encapsulate(url);
  
  this.execute=function(){
    if(this.onload===undefined)
      throw 'Processor: onload not defiend';
    
    if(typeof url ==='ArrayBuffer'){
      encapsulate(url);
      console.warn('Processor: No need to call execute on an ArrayBuffer');
      this.onload(this);
    }else{
        var request=new XMLHttpRequest();
        request.open("GET",url,true);
        request.responseType="arraybuffer";
    
      request.onreadystatechange=function(Event){
        if(request.readyState==4){
          if(request.status==0||request.status==200){
            buffer=request.response;
        
          var sizeArray=new Uint8Array(buffer);
          size=sizeArray.byteLength;
        
          this.onload(this);
        }else
          console.error("Processor: request error "+request.status);
        }
      
      }.bind(this);
      request.send();
    }
  }
  
  this.endian=function(endian){
    if(endian=='big')
	    endianness=false;
  	else
	    endianness=true;
  }
  
  this.tell=function(){
    return offset;
  }
  
  this.seek=function(off,abs){
    if(abs===true){
      if(off<size&&off>=0)
        offset=off;
      else 
        throw "Processor: offset out side of buffer bounds";
    }else{
      if((off+offset)<size&&(off+offset)>=0)
        offset+=off;
      else 
        throw "Processor: offset out side of buffer bounds";
    }
  }
  
  this.length=function(){
    return size;
  }
  
  this.slice=function(length,encapsulate){
    var s=buffer.slice(offset,offset+length);
    offset+=length;
    if(encapsulate==true){
      var newProc=new Processor(s);
      newProc.endian(endianness?'little':'big');
      return newProc;
    }
    return s;
  }

  this.splice=function(spliceData,length){
    if(typeof length !=='number')
      length=spliceData.length();
    if(length<0)
      throw "Processor: offset out side of buffer bounds";
    if(offset+length>size)
      throw "Processor: offset out side of buffer bounds";
      
    var tmp=spliceData.slice(length);
    
    if(offset%4){//Test for byte alignment.
      var oneByteLength=length;
      
      var oneByteFrom=new Uint8Array(tmp,0,oneByteLength);
      var oneByteTo=new Uint8Array(buffer,offset,oneByteLength);
      
      for(var i=0;i<oneByteLength;i++)
        oneByteTo[i]=oneByteFrom[i]; 
    }else{
      var fourByteLength=Math.floor(length/4);
      var oneByteLength=length%2;
      
      var fourByteFrom=new Uint32Array(tmp,0,fourByteLength);
      var fourByteTo=new Uint32Array(buffer,offset,fourByteLength);
      
      var oneByteFrom=new Uint8Array(tmp,fourByteLength*4,oneByteLength);
      var oneByteTo=new Uint8Array(buffer,offset+fourByteLength*4,oneByteLength);
      
      for(var i=0;i<fourByteLength;i++)
        fourByteTo[i]=fourByteFrom[i];
      for(var i=0;i<oneByteLength;i++)
        oneByteTo[i]=oneByteFrom[i]; 
    }
    offset+=length;
  }
  this.blob=function(mime){
    return new Blob([buffer],{type:mime});
  }
  this.typeSize={'Uint8':1,'Int8':1,
                'Uint16':2,'Int16':2,
                'Uint32':4,'Int32':4,
                'Float32':4,'Float64':8,
                'Urational64':8,'Rational64':8};
                
  this.processStructure=function(struct){
    var output=new Object();
	  var view=new DataView(buffer);
    for(var key in struct){
      var val;

      switch(struct[key]){
        case 'Uint8':
          val=view.getUint8(offset);
          offset++;
          break;
        case 'Int8':
          val=view.getInt8(offset);
          offset++;
          break;
        case 'Uint16':
          val=view.getUint16(offset,endianness);
          offset+=2;
          break;
        case 'Int16':
          val=view.getInt16(offset,endianness);
          offset+=2;
          break;
        case 'Uint32':
          val=view.getUint32(offset,endianness);
          offset+=4;
          break;
        case 'Int32':
          val=view.getInt32(offset,endianness);
          offset+=4;
          break;
        case 'Float32':
          val=view.getFloat32(offset,endianness);
          offset+=4;
          break;
        case 'Float64':
          val=view.getFloat64(offset,endianness)
          offset+=8;
          break;
        case 'Urational64'://Required by TIFF spec
          numerator=view.getUint32(offset,endianness);
          denominator=view.getUint32(offset+4,endianness);
          val=numerator/denominator;
          offset+=8;
          break;
        case 'Rational64'://Required by TIFF spec
          numerator=view.getInt32(offset,endianness);
          denominator=view.getInt32(offset+4,endianness);
          val=numerator/denominator;
          offset+=8;
          break;
        default:
          if(struct[key].substr(0,6)=='String'){
            var max=parseInt(struct[key].substr(6),10);
            var val=0;
            var Buff=new Array;

            if(Number.isNaN(max))
              max=-1;

            do{
              val=view.getUint8(offset++,endianness);
              Buff.push(val);
              if(max<0&&val==0)
                break;
            }while(--max!=0);
            for(var i=Buff.length-1;Buff[i]==0;i--)
              Buff.pop();
            val=String.fromCharCode.apply(null,Buff);
          }else{
            throw "Processor: "+struct[key]+" Is not a valid type";
          }
          break;
      }
      struct[key]=val;

    }
	  output=struct;
    return output;
  }
  
  this.writeStructure=function(struct,data){
	  var view=new DataView(buffer);
    for(var key in struct){
      var val;

      switch(struct[key]){
        case 'Uint8':
          val=view.setUint8(offset,data[key]);
          offset++;
          break;
        case 'Int8':
          val=view.setInt8(offset,data[key]);
          offset++;
          break;
        case 'Uint16':
          val=view.setUint16(offset,data[key],endianness);
          offset+=2;
          break;
        case 'Int16':
          val=view.setInt16(offset,data[key],endianness);
          offset+=2;
          break;
        case 'Uint32':
          val=view.setUint32(offset,data[key],endianness);
          offset+=4;
          break;
        case 'Int32':
          val=view.setInt32(offset,data[key],endianness);
          offset+=4;
          break;
        case 'Float32':
          val=view.setFloat32(offset,data[key],endianness);
          offset+=4;
          break;
        case 'Float64':
          val=view.setFloat64(offset,data[key],endianness)
          offset+=8;
          break;
        default: 
          if(struct[key].substr(0,6)=='String'){ //FIXME - ASCII only. 
            var max=parseInt(struct[key].substr(6),10);
            var val=data[key];
            var Buff;
            
            if(Number.isNaN(max))
              val=val.substr(0,max);

          //  Buff=TextEncoder().encode(val);
            for(var i=0;i<val.length;i++){
         //     view.setUint8(offset+i,Buff[i]);
              view.setUint8(offset+i,val.charCodeAt(i));//charCodeAt SUCKS!!
            }
            offset+=val.length;
          }else{
            throw "Processor: "+struct[key]+" Is not a valid type";
          }
          break;
      }
    }
  }  
}