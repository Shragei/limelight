//Copyright (c) James Schroer, 2014 
function JNG(url,targetNodeID,callback){
  var CRC=new CRC32();
  var signature=new Processor(new ArrayBuffer(8));//PNG signature
  signature.writeStructure({a:'Uint32',b:'Uint32'},
                           {a:0x89504e47,b:0x0d0a1a0a});
  var JNGsignature=new Processor(new ArrayBuffer(8));//JNG signature
  JNGsignature.writeStructure({a:'Uint32',b:'Uint32'},
                              {a:0x894a4e47,b:0x0d0a1a0a});                         
  var metadata=new Object;
  var ImageStack=new Array;
  var stack=new Array;
  var aux={head:new Array,tail:new Array};
  var chunkTypes={
    'IEND':readIEND,
    'IDAT':readIDAT,
    'PLTE':readPLTE,
    'JHDR':readJHDR,
    'JDAT':readJDAT
    
  };
  
  var imageState={};
  var image=new Processor(url);
  
  function chunkRead(image){
    var chunkStart=image.tell();
    var chunk=image.processStructure({Length:'Uint32',Type:'String4'});
    image.seek(chunk.Length,false);
    chunk.CRC=image.processStructure({CRC:'Uint32'}).CRC;    
    var chunkEnd=image.tell();
    image.seek(chunkStart,true);
    chunk.data=image.slice(chunkEnd-chunkStart,true);
    return chunk;
  }

  function writeBuff(chunkData,buffer,offset){
    chunkData.seek(0,true);
    var chunkbuff=chunkData.slice(chunkData.length());
    chunkbuff=new Uint8Array(chunkbuff);
    for(var i=0;i<chunkbuff.length;i++)
      buffer[offset++]=chunkbuff[i];
    return offset;
  }
  

  function readJHDR(chunk){
    if(stack.length!=0) //JHDR needs to be first. 
      return false;
    
    metadata.signature='JNG';
    
    chunk.data.seek(8,true);
    metadata=chunk.data.processStructure({Width:'Uint32',
                                          Height:'Uint32',
                                          ColorType:'Uint8',
                                          BitDepth:'Uint8',
                                          CompressMethod:'Uint8',
                                          InterlaceMethod:'Uint8',
                                          AlphaBitDepth:'Uint8',
                                          AlphaCompressMethod:'Uint8',
                                          AlphaFilterMethod:'Uint8',
                                          AlphaInterlaceMethod:'Uint8'});
    
    switch(metadata.ColorType){
      case 0:metadata.ColorType='Grayscale';break;
      case 10:metadata.ColorType='RGB';break;
      case 12:metadata.ColorType='Grayscale+Alpha';break;
      case 14:metadata.ColorType='RGB+Alpha';break;
      default:metadata.ColorType='Unknown';break;
    }
    
    switch(metadata.CompressMethod){
      case 8:metadata.CompressMethod='Huffman-coded baseline';break
      default:metadata.CompressMethod='Unknown';break;
    }
    switch(metadata.InterlaceMethod){
      case 0:metadata.InterlaceMethod='Sequential';break;
      case 1:metadata.InterlaceMethod='Progressive';break;
      default:metadata.InterlaceMethod='Unknown';break;
    }
    
    switch(metadata.AlphaCompressMethod){
      case 0:metadata.AlphaCompressMethod='PNG grayscale';break;
      case 8:metadata.AlphaCompressMethod='JNG 8bit grayscale';break;
      default:metadata.AlphaCompressMethod='Unknown';break;
    }
    
    switch(metadata.AlphaFilterMethod){
      case 0:metadata.AlphaFilterMethod='Adaptive';break
      default:metadata.AlphaFilterMethod='Unknown';break;
    }
    switch(metadata.AlphaInterlaceMethod){
      case 0:metadata.AlphaInterlaceMethod='None';break;
      default:metadata.AlphaInterlaceMethod='Unknown';break;
    }
    return true;
  }
  function readIEND(chunk){
    return false;
  }
  function readIDAT(chunk){
    if(stack.length==0)
      return false;
    
    var set=1;
    if(stack.indexOf('JSEP')>-1)//Switch from 8bit image to 12bit
      set=3;
    
    if(!(ImageStack[set] instanceof Array))    
      ImageStack[set]=new Array;
      
    ImageStack[set].push(chunk);
    
    return true;
  }
  function readJDAT(chunk){
    if(stack.length==0)
      return false;
    var set=0;
    if(stack.indexOf('JSEP')>-1)//Switch from 8bit image to 12bit
      set=2;
    
    if(!(stack[set] instanceof Array))    
      ImageStack[set]=new Array;
      
    ImageStack[set].push(chunk);

    return true;  
  }
  function readJDAA(chunk){
    if(stack.length==0)
      return false;
    var set=1;
    if(stack.indexOf('JSEP')>-1)//Switch from 8bit image to 12bit
      set=3;
    
    if(!(stack[set] instanceof Array))    
      ImageStack[set]=new Array;
      
    ImageStack[set].push(chunk);

    return true;    
  }

  function readPLTE(chunk){
    if(stack.legnth==0||metadata.signature=='JNG')
      return false;
    aux.head.push(chunk);
    return true;
  }


  function rebuildIHDR(newMetadata){
    var newIHDR=new Processor(new ArrayBuffer(25));
    
    
    switch(newMetadata.ColorType){
      case 'Grayscale':newMetadata.ColorType=0;break;
      case 'RGB':newMetadata.ColorType=2;break;
      case 'Palette':newMetadata.ColorType=3;break;
      case 'Grayscale+Alpha':newMetadata.ColorType=4;break;
      case 'RGB+Alpha':newMetadata.ColorType=6;break;
    }

    switch(newMetadata.CompressMethod){
      case 'deflate':newMetadata.CompressMethod=0;break
    }
    
    switch(newMetadata.FilterMethod){
      case 'Adaptive':newMetadata.FilterMethod=0;break
    }
    switch(newMetadata.InterlaceMethod){
      case 'None':newMetadata.InterlaceMethod=0;break;
      case 'Adam7':newMetadata.InterlaceMethod=1;break;
    }
    
    newMetadata.Length=13;
    newMetadata.Type='IHDR';
    
    newIHDR.writeStructure({Length:'Uint32',
                            Type:'String4',
                            Width:'Uint32',
                            Height:'Uint32',
                            BitDepth:'Uint8',
                            ColorType:'Uint8',
                            CompressMethod:'Uint8',
                            FilterMethod:'Uint8',
                            InterlaceMethod:'Uint8'},newMetadata);
    newIHDR.seek(4,true);
    var crc=CRC.Compute(newIHDR.slice(newIHDR.length()-8));
    newIHDR.writeStructure({CRC:'Uint32'},{CRC:crc});
    newIHDR.seek(0,true);
    return newIHDR;
  }
  function rebuildIDAT(chunk){
    var data=chunk.data;
    data.seek(8,false);
    chunk.SequenceNumber=data.processStructure({SeqNum:'Uint32'})
    data.seek(4,true);
    data=data.slice(data.length()-4,true);//strip the fdAT sequence_number from the beginning of chunk
    
    chunk.Length-=4;
    
    data.writeStructure({Length:'Uint32',Type:'String4'},{Length:chunk.Length,Type:'IDAT'});//make image chunk valid
    data.seek(4,true); //reset to past length;
    chunk.CRC=CRC.Compute(data.slice(data.length()-8)); //slice everything to crc
    data.writeStructure({CRC:'Uint32'},{CRC:chunk.CRC}); //replace crc with new one
    chunk.data=data;
  }
  

  function readPNGStructure(){
    var ret;
    do{
      var chunk=chunkRead(image);
      var call=chunkTypes[chunk.Type];
      if(call!==undefined)
        ret=call(chunk);
      stack.push(chunk.Type);
    }while(ret);
  }
  
  function buildChunk(meta,data){
    var length=0;
    if(data)
      length=data.length();
    
    var chunk=new Processor(new ArrayBuffer(length+12));
    chunk.writeStructure({Length:'Uint32',Type:'String4'},meta);
   
    if(data)
      chunk.splice(data);

    chunk.seek(4,true);

    var crc=CRC.Compute(chunk.slice(chunk.length()-8));
    chunk.writeStructure({CRC:'Uint32'},{CRC:crc});
    chunk.seek(0,true);
    return chunk;
  }
  function rebuildImage(chunks,meta){
    var initSize=8+25+12; //signature+IHDR+IEND
    
    for(var i=0;i<aux.head.length;i++)
      initSize+=aux.head[i].data.length();
    for(var i=0;i<aux.tail.length;i++)
      initSize+=aux.tail[i].data.length();
      
    var size=initSize;
      
    for(var c=0;c<chunks.length;c++)//calculate chunk size
      size+=chunks[c].data.length();
      
    var buffer=new Processor(new ArrayBuffer(size));
    signature.seek(0,true);
    buffer.splice(signature);
    
    var newIHDR=rebuildIHDR(meta);
    buffer.splice(newIHDR);
    
    for(var i=0;i<aux.head.length;i++){
      aux.head[i].data.seek(0,true);
      buffer.splice(aux.head[i].data);
    }
      
    for(var c=0;c<chunks.length;c++){
      chunks[c].data.seek(0,true);
      buffer.splice(chunks[c].data);
    }
    
    for(var i=0;i<aux.tail.length;i++){
      aux.tail[i].data.seek(0,true);
      buffer.splice(aux.tail[i].data);
    }
    
    var end=buildChunk({Length:0,Type:'IEND'});
    buffer.splice(end);
    return buffer;
  }

  function JNGRebuild(){
    var set=0;
    function rebuildJPEG(chunks){
      var jpegSize=0;
      for(var c=0;c<chunks.length;c++)
        jpegSize=chunks[c].Length;
      var jpegBuffer=new Processor(new ArrayBuffer(jpegSize));
      
      for(var c=0;c<chunks.length;c++){
        chunks[c].data.seek(8,true);//skip the chunk header
        jpegBuffer.splice(chunks[c].data.slice(chunks[c].Length,true));
      }
      return jpegBuffer;
    }
    var set=0;
    do{
      ImageStack[set]=rebuildJPEG(ImageStack[set]);
      
      if(metadata.ColorType=='Grayscale+Alpha'||
         metadata.ColorType=='RGB+Alpha'){
        if(metadata.AlphaCompressMethod=='PNG grayscale'){
          var meta={Width:metadata.Width,
                    Height:metadata.Height,
                    BitDepth:metadata.AlphaBitDepth,
                    ColorType:'Grayscale',
                    CompressMethod:'deflate',
                    FilterMethod:metadata.AlphaFilterMethod,
                    InterlaceMethod:metadata.AlphaInterlaceMethod};
          ImageStack[set+1]=rebuildImage(ImageStack[set+1],meta);
        }else{
          ImageStack[set+1]=rebuildJPEG(ImageStack[set=1]);
        }
         
      }
      set+=2;
    }while(metadata.BitDepth==20&&set<=2);
  }
  
  this.GetImage=function(){
    return image;
  }
  
  image.onload=function(){
    image.seek(8,true);//Skip PNG header
    
    readPNGStructure();
   // console.log(metadata);
   JNGRebuild();

    var loaded=1;
    var priImg=new Image;
    var alphaImg=new Image;
    priImg.src=URL.createObjectURL(ImageStack[0].blob('image/jpeg'));
    priImg.onload=onload;
    if(metadata.ColorType=='Grayscale+Alpha'||
       metadata.ColorType=='RGB+Alpha'){
      loaded++;
      var mime=metadata.AlphaCompressMethod=='PNG grayscale'?'image/png':'image/jpeg';
   //   var imgData=ImageStack[1].blob('application/octet-stream');
   //   document.location=URL.createObjectURL(imgData);
      alphaImg.src=URL.createObjectURL(ImageStack[1].blob(mime));
      alphaImg.onload=onload;
    }
    
    function onload(){
      if(--loaded==0){
        var image=document.createElement('canvas');
        image.height=metadata.Height;
        image.width=metadata.Width;
        image.style.height=image.height;
        image.style.width=image.width;
        var imgContext=image.getContext('2d');
        imgContext.drawImage(priImg,0,0,metadata.Width,metadata.Height);
        if(metadata.ColorType=='Grayscale+Alpha'||
           metadata.ColorType=='RGB+Alpha'){
          var alpha=document.createElement('canvas');
          alpha.height=metadata.Height;
          alpha.width=metadata.Width;
          alpha.style.height=image.height;
          alpha.style.width=image.width;
          var alphaContext=alpha.getContext('2d');
          alphaContext.drawImage(alphaImg,0,0,metadata.Width,metadata.Height);
          var imgData=imgContext.getImageData(0,0,metadata.Width,metadata.Height);
          var alphaData=alphaContext.getImageData(0,0,metadata.Width,metadata.Height);
          for(var i=3;i<imgData.data.length;i+=4){
            imgData.data[i]=alphaData.data[i-3];
          }
          imgContext.putImageData(imgData,0,0);
        }
        var target=document.getElementById(targetNodeID)
      target.parentNode.insertBefore(image,target);
      target.parentNode.removeChild(target);
      }
    }

    if(callback)
      callback();
    console.log('Frame cound: '+ImageStack.length);
  }
  
  
  image.execute();
}