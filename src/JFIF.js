//Copyright (c) James Schroer, 2014 
function JFIF(){
  var initated=false;
  var AppSegments={};
  var MarkerMap=[];
  this.execute=function(proc){
    for(var marker=proc.processStructure({Marker:'Uint16'}).Marker;
        marker!=parseInt('FFD9',16); //EOI - End of Image
        marker=proc.processStructure({Marker:'Uint16'}).Marker){
        
      
      
      if(marker==parseInt('FFD8',16)){ //SOI - Start of Image
        if(initated)
          throw "Image coruption";
        initated=true;
      }
      if(initated!=true)
        throw "Image coruption";
      if(marker<parseInt('FF01',16)){
        proc.seek(-1,false);
        continue;
      }
      
      MarkerMap.push({Marker:marker,Offset:proc.tell()-2});
      if((marker>=parseInt('FFD0',16)&&marker<=parseInt('FFD7',16))|| //standlone markers with no length
          parseInt('FF01',16)==marker||parseInt('FFD8',16)==marker)
        continue;
        
      var appLength=proc.processStructure({Length:'Uint16'}).Length;
      if(marker>=parseInt('FFE0',16)&&marker<=parseInt('FFEF',16)){ //APP0 - APP15 markers
        
        var identStart=proc.tell();
        var identifier=proc.processStructure({Identifier:'String'}).Identifier;

        appLength-=(proc.tell()-identStart);
        
        console.log('Marker: '+marker.toString(16)+' Offset: '+(proc.tell()-8)+' Length: '+appLength+' Identifier: '+identifier);
        if(marker in AppSegments&&identifier in AppSegments[marker]){
          var absOffset=proc.tell();
          var AppSegmentsProc=proc.slice(appLength-2,true);
          AppSegments[marker][identifier](AppSegmentsProc,absOffset);
        }else
          proc.seek(appLength-2,false);
      }else{//JFIF markers with a length.
        console.log('Marker: '+marker.toString(16)+' Offset: '+(proc.tell()-4)+' Length: '+appLength);
        proc.seek(appLength-2,false);
      }      
    }
  }
  
  this.AppSegment=function(appsegment,identifer,callback){
    var marker=parseInt('FFE0',16);
    var i=0;
    
    for(;'APP'+i!=appsegment;i++){
      if(i>15)
        throw "Appsegment not valid";
    }
    marker+=i;
    
    if(!(marker in AppSegments))
      AppSegments[marker]={};
    
    AppSegments[marker][identifer]=callback;
  }
  
  this.insertAppSegment=function(postion,appsegment,identifer,data){
    var marker=parseInt('FFE0',16);
    var i=0;
    
    for(;'APP'+i!=appsegment;i++){
      if(i>15)
        throw "Appsegment not valid";
    }
    marker+=i;
  }
}
