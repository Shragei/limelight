//Copyright (c) James Schroer, 2014 
$(document).ready(function(){
  var mpo=new Processor('lighter.mpo');
  var ImageStack=[];
  mpo.onload=function(mpo){
   var jfif=new JFIF();
   jfif.AppSegment('APP2','MPF',function(proc,absOffset){
      var tiff=new TIFF();
      var indexCount=0;
      tiff.Tags('B000',function(datatype,count,value){
        var version=value.processStructure({Version:'String4'}).Version;
        console.log('MPF Version: '+version);
      });
      tiff.Tags('B001',function(datatype,count,value){
        indexCount=value;
        console.log('MPF Number of Images: '+value);
      });
      tiff.Tags('B002',function(datatype,count,value){
        for(var i=0;i<indexCount;i++){
          var entry=value.processStructure({Attributes:'Uint32',Size:'Uint32',Offset:'Uint32',
                                           Dependent1:'Uint16',Dependent2:'Uint16'});
          var attributes={};
          attributes.Child=entry.Attributes&parseInt('00000080',16);
          attributes.Parent=entry.Attributes&parseInt('00000040',16);
          attributes.Representative=entry.Attributes&parseInt('00000040',16);
          attributes.Format=entry.Attributes&parseInt('00000007',16);
          if(entry.Offset)
            entry.Offset+=absOffset;
          console.log('Size: '+entry.Size+' Offset: '+entry.Offset+' Dep1: '+entry.Dependent1+' Dep12 '+entry.Dependent2);
        }
      });

     console.group('TIFF');
     tiff.execute(proc);
     console.groupEnd();
   });
   jfif.execute(mpo);
   console.log('finished: '+mpo.tell());
  }
  mpo.execute();
});

