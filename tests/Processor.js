module("Processor");
var data=new ArrayBuffer(32);
var uint8=new Uint8Array(data);
uint8.set([0x57,0x81,0x85,0x26,0x9d,0x51,0xf5,0x82,0x7a,0x4d,0x12,0x06,0x5d,0x8f,0xdb,0x19,0x8e,0xfd,0xcd,0x93,0x3b,0x3d,0x0d,0x08,0x95,0x00,0xf7,0xae,0x31,0xc0,0x05]);
test('Seek',function(){
  var processor=new Processor(data);
  processor.seek(5,true);
  equal(processor.tell(),5,"Absolute seek");
  
  processor.seek(5,false);
  equal(processor.tell(),10,"Relative seek");
  
  throws(function(){
    processor.seek(-10,true);
  },"Throws lower bounds error when seeking passed limits");
  
  throws(function(){
    processor.seek(33,true);
  },"Throws upper bounds error when seeking passed limits");
});

test('Length',function(){
 var processor=new Processor(0);
 equal(processor.length(),0,"Length zero");
 
 var processor=new Processor(10);
 equal(processor.length(),10,"Length");
});
test('processStructure unsigned',function(){
  var processor=new Processor(data);
  var unsigned=processor.processStructure({Uint16:'Uint16',Uint32:'Uint32',Uint8:'Uint8'});
  equal(unsigned.Uint8,245,"Uint8");
  equal(unsigned.Uint16,22401,"Uint16");
  equal(unsigned.Uint32,2233900369,"Uint32");
});

test('writeStructure unsigned',function(){
  var data=new ArrayBuffer(7);
  var processor=new Processor(data);
  var unsigned=processor.writeStructure(
    {Uint16:'Uint16',Uint32:'Uint32',Uint8:'Uint8'},
    {Uint16:5321,Uint32:245621,Uint8:193});
  var results=new Uint8Array(data);
  var test=new Uint8Array(7);
  test.set([20,201,0,3,191,117,193]);
  deepEqual(results,test,"Uint8, Uint16, Uint32");
});
test('processStructure signed',function(){
  var processor=new Processor(data);
  var signed=processor.processStructure({Int16:'Int16',Int32:'Int32',Int8:'Int8'});
  equal(signed.Int8,-11,"Int8");
  equal(signed.Int16,22401,"Int16");
  equal(signed.Int32,-2061066927,"Int32");
});
test('writeStructure signed',function(){
  var data=new ArrayBuffer(7);
  var processor=new Processor(data);
  var unsigned=processor.writeStructure(
    {Int16:'Int16',Int32:'Uint32',Int8:'Uint8'},
    {Int16:-5334,Int32:87364,Int8:-72});
  var results=new Uint8Array(data);
  var test=new Uint8Array(7);
  test.set([235,42,0,1,85,68,184]);
  deepEqual(results,test,"Int8, Int16, Int32");
});
test('processStructure float',function(){
  var processor=new Processor(data);
  var float=processor.processStructure({Float32:'Float32',Float64:'Float32'});
  equal(float.Float32,284817736335360,"Float32");
  equal(float.Float64,-2.7787844247979685e-21,"Float64");

});
test('writeStructure float',function(){
  var data=new ArrayBuffer(12);
  var processor=new Processor(data);
  processor.writeStructure({Float32:'Float32',Float64:'Float32'},
                                       {Float32:284817736335360,Float64:-2.7787844247979685e-21});
  var results=new Uint8Array(data);
  var test=new Uint8Array(12);
  test.set([87,129,133,38,157,81,245,130,0,0,0,0]);
  deepEqual(results,test,"Float32, Float64");

});